OS Client UH-IAAS
=========

A repeatable role to provision Openstack resources from UH-IAAS. Provides default values for the UH-IAAS Cloud.
Runs on 'localhost' to provision resources for playbooks.


Requirements
------------
Installed packages from https://git.app.uib.no/Oyvind.Gjesdal/ansible-jumphost:

```
python3-openstacksdk, python3-openstackclient
```

Uses OS env variables or vaulted variables to set OS credentials.

Role Variables
--------------

Variables that need to be set are
 
```
os_client_region_name #('bgo' or 'osl')`
os_client_instance_name`: # alphanumeric_prod|test 
os_client_key:
  name: "ansible_key"
  path: "/home/fedora/.ssh/key.pub"
```

Dependencies
------------
Depends on OS environment variables sourced from keystone.rc.sh [NREC documentation for OS_CLI](https://docs.nrec.no/api.html#using-the-cli-tools)
```
source <(ansible-vault decrypt --vault-id ~/secrets/wab ~/keystone.rc.sh --output -))
```
or overriding variables from default.yml (preferably with vaulted values)

adding values for variables:  
```
vault_os_client_username: 
vault_os_client_project_name: 
vault_os_client_password: # api-key
```
`host_vars/localhost/vars.yml` with default values:
```
os_client_auth_url: "https://api.nrec.no:5000/v3"
os_client_identity_api_version: 3
os_client_user_domain_name: "dataporten"
os_client_project_domain_name: "dataporten"
os_client_region_name: # osl or bgo, set in vars section of role. 
os_client_no_cache: "1"
# set vaulted values in vault.yml
os_client_username:  "{{vault_os_client_username}}" #institutional email, same as dataporten
os_client_project_name: "{{ vault_os_client_project_name }}" # project name
os_client_password: "{{ vault_os_client_password }}" # api token, can be reset at uh-iaas web pages if lost. 
```
`os_client_env` should be added to environment in top of Playbook, see example:

Example Playbook
----------------
```
- name: "provision server"
  hosts: localhost
  environment: "{{ os_client_env }}"

  tasks:
  - set_fact:
      os_client_instance_name: "instance1_prod"
  
  - name: "import role uh-iaas-client"
    import_role:
      name: "os_client_uh-iaas"
    vars:
      os_client_rebuild: false
      os_client_flavor: "m1.large"
      os_client_region_name: "bgo"
      os_client_key:
        name: "ansible_key"
        path: "/path/to_pub/id_rsa.pub"
      os_client_hostfile_sections:
      - "{{ os_client_instance_name }}"
      - "database"
      os_client_security_groups: # See note on silent error on unknown security group
      - "ssh and icmp uib"
      - "http uib"
      - "default"
      os_client_volumes:
      - display_name: "mariadb"
        size: "20"
        device: "/dev/sdb"
      - display_name: "letsencrypt"
        size: "1"
        device: "/dev/sdc"
        type: "mass-storage-ssd" See https://docs.nrec.no/manage-volumes.html#create-a-volume for available volume types
```

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
